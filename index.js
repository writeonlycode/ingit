#!/usr/bin/env node

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import inquirer from "inquirer";
import { createSpinner } from "nanospinner";
import { simpleGit } from "simple-git";

import os from "os";
import path from "path";
import * as fs from "fs";

yargs(hideBin(process.argv)).scriptName("ingit").command({
  command: "$0 <user/repository> [directory]",
  desc: "download the master branch of a repository from GitLab",
  builder: defaultCommandBuilder,
  handler: defaultCommandHandler,
}).argv;

function defaultCommandBuilder(yargs) {
  yargs.positional("user/repository", {
    describe: "repository to download content from",
    type: "string",
  });

  yargs.positional("directory", {
    describe: "directory to download content to",
    type: "string",
    default: ".",
  });
}

async function defaultCommandHandler(argv) {
  const temporary = fs.mkdtempSync(path.join(os.tmpdir(), "ingit"));
  const repository = `git@gitlab.com:${argv["user/repository"]}.git`;
  const directory = argv["directory"];

  // Check if directory is empty and confirm before overwrite
  if (fs.existsSync(directory) && fs.readdirSync(directory).length) {
    const answers = await inquirer.prompt({
      name: "overwrite",
      type: "confirm",
      message:
        "The destination directory already exists. Do you want to overwrite the files there?",
    });

    if (!answers.overwrite) {
      return;
    }
  }

  // Clone repository to temporary directory
  const spinner = createSpinner("Cloning repository...").start();

  const progress = ({ method, stage, progress }) => {
    spinner.update({ text: `Cloning repository (${stage}): ${progress}%` });
  };

  try {
    await simpleGit({ progress }).clone(repository, temporary, { '--depth': 1 });
  } catch (e) {
    spinner.error({ text: "Cloning repository: Failure!" });
    throw e;
  }

  // Remove .git folder from temporary directory
  const gitFolder = path.join(temporary, ".git");

  try {
    fs.rmSync(gitFolder, { recursive: true, force: true });
  } catch (e) {
    spinner.error({ text: "Cloning repository: Failure!" });
    throw e;
  }

  // Copy files to directory
  try {
    fs.cpSync(temporary, directory, { recursive: true });
  } catch (e) {
    spinner.error({ text: "Cloning repository: Failure!" });
    throw e;
  }

  spinner.success({ text: "Cloning repository: Success!" });
}
