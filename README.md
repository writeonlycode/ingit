# Ingit

A simple project scaffolding that uses git.

## Installation

```bash
node install -g ingit
```

## Usage

```bash
ingit <user/repository> [directory] 
```
